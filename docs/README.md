---
home: true
heroImage: /hero.png
features:
- title: Collaboration and Code Review Practices...
  details: Explore best practices for collaborative development using Git. Dive into effective code review techniques, leveraging tools and workflows to ensure code quality and team synchronization.
- title: Branching Strategies Demystified
  details: Explore the principles and advantages of trunk-based development. Understand how it fosters collaboration, reduces merge conflicts, and enables faster release cycles.
- title: CI/CD Integration
  details: Implement and optimize CI/CD pipelines using GitLab. Learn to automate builds, testing, and deployment processes, leveraging GitLab's features to ensure robust continuous integration and delivery workflows.
footer: CC BY 4.0 | Jan Simson
---